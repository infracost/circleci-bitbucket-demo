# Infracost CircleCI with Bitbucket Demo

See [this pull request](https://bitbucket.org/infracost/circleci-bitbucket-demo/pull-requests/2) for the demo.

Follow [our documentation here](https://github.com/infracost/infracost-circleci) to setup Infracost in your CircleCI pipeline to see cost estimates in your pull requests.

See the [Infracost integrations](https://www.infracost.io/docs/integrations/cicd) page for other integrations.
